const readline = require('readline')
const _log = console.log

module.exports = class Terminal{
	constructor(hook){
		if(hook){
			if(_log != console.log) throw new Error('console.log is already hooked')
			console.log = this.log.bind(this)
			this.hooked = true
		}
		this.rl = readline.createInterface({
			input: process.stdin,
			output: process.stdout
		})
		this.listeners = []
		this.rl.on('line', input=> this.listeners.forEach(l=> l(input)))
	}
	log(){
		this.rl.pause() //no typing now, shh
		readline.clearLine(process.stdout, 0) //remove artifacts
		readline.cursorTo(process.stdout, 0) //remove blank spaces

		const result = _log(...arguments) //makes this work even if hooked
		this.rl.prompt(true) //bring back artifacts
		return result
	}
	listen(listener){
		this.listeners.push(listener)
		return this
	}
	unlisten(listener){
		this.listeners.splice(this.listeners.indexOf(listener), 1)
		return this
	}
	close(){
		this.rl.close()
		if(this.hooked) console.log = _log
	}
}
