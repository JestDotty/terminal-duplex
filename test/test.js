const assert = require('assert')
const Terminal = require('../index')

let terminal
afterEach(()=> terminal.close())

//manual test is best
it.skip('manual', function(done){
	terminal = new Terminal()
	const listener = input=> terminal.log('user inputted:', input)
	terminal.listen(listener)

	const noiser = setInterval(()=> terminal.log('noise'), 100)
	setTimeout(()=>{
		clearInterval(noiser)
		terminal.unlisten(listener)
		done()
	}, 3000)
})

//these work on some machines but not others.
it.skip('clean', function(done){
	terminal = new Terminal()
	const listener = input=> terminal.log('user inputted:', input)
	terminal.listen(listener)

	const noiser = setInterval(()=> terminal.log('noise'), 25)
	const user = setInterval(()=>{
		terminal.rl.write('user')
		assert(terminal.rl.line, 'user')
		terminal.rl.write('\n')
	}, 60)
	setTimeout(()=>{
		clearInterval(noiser)
		clearInterval(user)
		terminal.unlisten(listener)
		done()
	}, 300)
})
it.skip('dirty', function(done){
	terminal = new Terminal(true)
	const listener = input=> console.log('user inputted:', input)
	terminal.listen(listener)

	const noiser = setInterval(()=> console.log('noise'), 25)
	const user = setInterval(()=>{
		terminal.rl.write('user')
		assert(terminal.rl.line, 'user')
		terminal.rl.write('\n')
	}, 60)
	setTimeout(()=>{
		clearInterval(noiser)
		clearInterval(user)
		terminal.unlisten(listener)
		done()
	}, 300)
})

it('too hooked error', ()=>{
	terminal = new Terminal(true)
	assert.throws(()=> new Terminal(true), /console\.log is already hooked/)
})
